.. _gsoc-2021-projects:

:far:`calendar-days` 2021
#########################

Improvements to simpPRU
************************

.. youtube:: -zZ57sqQPSY
   :width: 100%

| **Summary:** Building upon the work done in GSoC 2020 to make programming the PRU easier for beginners, this project aims to add tests, features, and more compatibility for simpPRU, the simple Python-like language that compiles to PRU C and runs natively on the PRU. 

**Contributor:** Archisman Dey

**Mentors:** Pratim Ugale, Andrew Henderson, Abhishek Kumar

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2021/projects/6216005071667200
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2021_Proposal/simpPRU_Improvements
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

MicroPython for BeagleConnect Freedom
**************************************

.. youtube:: x1hBazDr2Bk
   :width: 100%

| **Summary:** The project includes porting MicroPython and CircuitPython support to the BeagleConnect Freedom Device as well as writing drivers for interfaces like GPIO, I2C, SPI, PWM, UART and ADC in Micropython.

**Contributor:** Yadnik Bendale 

**Mentors:** Deepak Khatri, Jason Kridner

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2021/projects/6255768214437888
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC_2021/Micropython_for_BeagleConnect_Freedom
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Beagle-Config
*************

.. youtube:: vFUWCzqE6xI
   :width: 100%

| **Summary:** Beagle-Config is a tool-set, that aims to provide the functionality to make the most common low-level configuration changes in beagle devices easily and providing a terminal UI to do the same as well as a host-side application for ICS and many one-click enable features for seamless user experience.

**Contributor:** Shreyas Atre

**Mentors:** Deepak Khatri, Arthur Sonzogni, Abhishek Kumar

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2021/projects/6276044352389120
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2021_Proposal/beagle_config
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

BeagleWire Software
********************

.. youtube:: X3gu4sAZo0I
   :width: 100%

| **Summary:** The BeagleWire is an FPGA development platform that has been designed for use with BeagleBone boards. BeagleWire is a cape on which there is an FPGA device (Lattice iCE40HX). The software support for BeagleWire is still in the development phase. 

**Contributor:** Omkar Bhilare

**Mentors:** Michael Welling, Shephen Arnold

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2021/projects/6597728544489472
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2021_Proposal/OmkarBhilare
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

GPGPU with GLES
****************

.. youtube:: I5FnOTc8OP8
   :width: 100%

| **Summary:** Since BeagleBoards are heterogeneous platforms, why not use them to their full extent? Apparently for the time being the GPU block lies mostly useless and cannot assist with any computations due to non-technical reasons. Normally, OpenCL would be used for accelerating on the GPU, but taking the limitations discussed above it is impossible, yet there is another way! GPGPU acceleration with OpenGLES.

**Contributor:** Jakub Duchniewicz

**Mentors:** Iain Hunter, Hunyue Yau

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2021/projects/5248746325016576
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2021_Proposal/GPGPU_with_GLES
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Bela Support for the BeagleBone AI
***********************************

.. youtube:: kLXCrlQwXeI
   :width: 100%

| **Summary:** As given on the `official website <https://learn.bela.io/get-started-guide/say-hello-to-bela/#what-is-bela>`_, Bela is a hardware and software system for creating beautiful interaction with sensors and sound. Bela consists of a Bela cape on top of a BeagleBone Black computer (uptil now). The idea is to enable smooth transition of the associated software to the BBAI and future TI chips.

**Contributor:** Dhruva Gole

**Mentors:** Giulio Moro, Stephen Arnold, Robert Manske

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/archive/2021/projects/5188984237457408
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2021_Proposal/bela_on_bbai
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip:: 

   Checkout eLinux page for GSoC 2021 projects `here <https://elinux.org/BeagleBoard/GSoC/2021_Projects>`_ for more details.