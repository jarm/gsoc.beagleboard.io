.. _gsoc-ideas-rtos-microkernel-improvements:

RTOS/microkernel imporvements
#############################

These project ideas focus on growing open source for microcontrollers with a 
specific focus on better understanding of computer systems and co-processing 
in a heterogeneous, asymmetrical multiprocessor system where Linux is the 
typical kernel used on the system coordination portion of the system. 

.. card:: 

    :fas:`timeline;pst-color-secondary` **RTEMS on RISC-V**
    ^^^^

    - **Goal:** Add RISC-V-based PolarFire SoC support to RTEMS RTOS
    - **Hardware Skills:** RISC-V
    - **Software Skills:** C, RTOS
    - **Possible Mentors:** Joel Sherrill, jkridner
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** `https://git.rtems.org <https://git.rtems.org/>`_
    - **References:**
        - `Issue on RTEMS tracker <https://devel.rtems.org/ticket/4626>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`timeline;pst-color-secondary` **Zephyr on R5/M4F (K3)**
    ^^^^

    - **Goal:** Add Zephyr RTOS support to the R5/M4F cores in the J721E/AM62 SoC
    - **Hardware Skills:** R5/M4F
    - **Software Skills:** C, RTOS
    - **Possible Mentors:** NishanthMenon, Vaishnav Achath
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** https://github.com/zephyrproject-rtos/zephyr
    - **References:**
        - `J721E TRM <http://www.ti.com/lit/pdf/spruil1>`_
        - `AM62 TRM <https://www.ti.com/lit/pdf/spruiv7>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 